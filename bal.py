#! ../env/bin/python
# encoding: utf-8
import struct
import time

from pymodbus.pdu import ModbusRequest
from pymodbus.pdu import ModbusResponse

import logging
log = logging.getLogger('BAL').addHandler(logging.NullHandler())

#функция для преобразования десятичного числа в 16ричное представление ввиде строки
shex = lambda num: '0x{:04X}'.format(num)


class CommandRequest(ModbusRequest):
    function_code = 66
    _rtu_frame_size = 8

    def __init__(self, cmd=None, device_type=None, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self.cmd = cmd
        self.device_type = device_type

    def encode(self):
        return struct.pack('>HH', self.cmd, self.device_type)

    def decode(self, data):
        self.cmd, self.device_type = struct.unpack('>HH', data)

    def getResponseSize(self):
        return 2

    def __str__(self):
        return "CommandRequest %s => %s" % (shex(self.cmd), shex(self.device_type))


class CommandResponse(ModbusResponse):
    function_code = 66
    _rtu_frame_size = 6

    def __init__(self, value=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self.value = value

    def encode(self):
        return struct.pack('>H',  self.value)

    def decode(self, data):
        self.value = struct.unpack('>H', data)[0]

    def __str__(self):
        return "CommandResponse %s" % shex(self.value)


from pymodbus.register_read_message import ReadRegistersResponseBase

class ReadLogRequest(ModbusRequest):
    function_code = int('0x47', 16)
    _rtu_frame_size = 4

    def __init__(self, **kwargs):
        ModbusRequest.__init__(self, **kwargs)

    def encode(self):
        return ''

    def decode(self, data):
        return ''

    def getResponseSize(self):
        return 21

    def __str__(self):
        return "ReadLogRequest %s" % (shex(self.address))


class ReadLogResponse(ReadRegistersResponseBase):
    function_code = int('0x47', 16)
    _rtu_frame_size = 25

    def __init__(self, value=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self.value = value

    def encode(self):
        ''' Encodes the response packet

        :returns: The encoded packet
        '''
        result = chr(len(self.registers) * 2)
        for register in self.registers:
            result += struct.pack('>B', register)
        return result

    def decode(self, data):
        ''' Decode a register response packet

        :param data: The request to decode
        '''
        byte_count = ord(data[0])
        self.registers = []
        for i in range(1, byte_count + 1):
            self.registers.append(struct.unpack('>B', data[i])[0])

    def __str__(self):
        return "ReadLogResponse %s" % self.value

from pymodbus.factory import ClientDecoder, ServerDecoder
ServerDecoder._ServerDecoder__function_table.append(CommandRequest)
ClientDecoder._ClientDecoder__function_table.append(CommandResponse)
ServerDecoder._ServerDecoder__function_table.append(ReadLogRequest)
ClientDecoder._ClientDecoder__function_table.append(ReadLogResponse)

import time

from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.register_read_message import ReadHoldingRegistersRequest as read
from pymodbus.register_write_message import WriteMultipleRegistersRequest as write
import os.path
cmd = CommandRequest
read_log = ReadLogRequest


#client = ModbusClient(method='rtu', port='/dev/ttyUSB0', parity='E',
                      #baudrate=57600, timeout=0.02)

from pymodbus.pdu import ExceptionResponse
from pymodbus.exceptions import ModbusException

class InvalidRequest(ValueError):
    pass

class ConverterNotConnected(Exception):
    pass

class Client(object):
    def __init__(self):
        self.port = None
        self.fast_client = None
        self.midl_client = None
        self.slow_client = None

    def close(self):
        if self.fast_client:
            self.fast_client.close()
            self.fast_client = None
        if self.midl_client:
            self.midl_client.close()
            self.midl_client = None
        if self.slow_client:
            self.slow_client.close()
            self.slow_client = None


    def connect(self):
        for adapter_path in ['/dev/ttyUSB0','/dev/ttyUSB1','/dev/ttyUSB2','/dev/ttyUSB3','/dev/ttyUSB4']:
            if os.path.exists(adapter_path):
                break
        else:
            raise ConverterNotConnected("USB converter isn't connected.")

        self.port = adapter_path

        # for ST: 
        #   short reads up to 15 registers (~0.015s)
        #   long reads up to 120 registers (~0.060s)
        #   short writes up to 30 registers (~0.018s)
        #   cmds (~0.01s)
        # for TM: 
        #   short reads up to 15 registers (~0.015s)
        #   long reads up to 30 registers (~0.017s)
        #   short writes up to 20 registers (~0.010s)
        #   cmds (~0.01s)
        self.fast_client = ModbusClient(method='rtu', port=self.port, parity='E',
                              baudrate=57600, timeout=0.200)

        # for ST: 
        #   writes up to 100 registers (~0.045s)
        # for TM: 
        #   writes up to 40 registers (~0.022s)
        self.midl_client = ModbusClient(method='rtu', port=self.port, parity='E',
                              baudrate=57600, timeout=0.500)
        ## for:
        ##   long writes up to 110 registers (~0.100s)
        self.slow_client = ModbusClient(method='rtu', port=self.port, parity='E',
                              baudrate=57600, timeout=1)

        #return self.fast_client, self.midl_client, self.slow_client
        return self.fast_client

    def _client_for(self, req):
        return self.fast_client

        if isinstance(req, write):
            if req.count < 0:
                raise ValueError('req.count is < 0?? WAT!?')
            if 0 < req.count <= 10:
                return self.fast_client
            elif 10 < req.count <= 40:
                return self.midl_client
            elif 40 < req.count < 110:
                return self.slow_client
            else:
                raise InvalidRequest("Can't write more than 110 registers per request")

        elif isinstance(req, read):
            if req.count < 0:
                raise ValueError('req.count is < 0?? WAT!?')
            if 0 < req.count <= 125:
                return self.fast_client
            else:
                raise InvalidRequest("Can't read more than 120 registers per request")

        elif isinstance(req, read_log):
            return self.slow_client

        elif isinstance(req, cmd):
            return self.fast_client

    def execute(self, req):
        time.sleep(0.01) #таймаут между послыками ~5 символов
        try:
            resp = self._client_for(req).execute(req)
        except ModbusException as e:
            print e
            return None

        if isinstance(resp, ExceptionResponse):
            print resp
            return None
        else:
            return resp


#client = Client(adapter_path)



if __name__ == '__main__':
    pass
    #client.connect()

    #for addr in range(0, 2):
        #startTs = time.time()
        #resp = client.read_holding_registers(addr, 1, unit=0xEA)
        #stopTs = time.time()
        #timeDiff = stopTs  - startTs
        #print timeDiff
        #for reg in resp.registers:
            #print shex(reg)


    #startTs = time.time()
    #resp = client.execute(read(0xC000, 8, unit=0x01))
    #stopTs = time.time()
    #timeDiff = stopTs  - startTs
    #print timeDiff
    #for reg in resp.registers:
        #print shex(reg)
