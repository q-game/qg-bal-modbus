#!/usr/bin/env bash

trap 'kill -TERM $PID' TERM INT
./start.py &
PID=$!
wait $PID
trap - TERM INT
wait $PID
EXIT_STATUS=$?
