#QGame Bal(modbus)
FROM ubuntu:14.04
MAINTAINER Sergey Anuchin "sergunich@gmail.com"

RUN apt-get install -y locales
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
RUN echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen en_US.UTF-8 ru_RU.UTF-8
ENV LC_ALL en_US.UTF-8


RUN ulimit -n 1024
RUN apt-get update && apt-get install -y \
        #common
        build-essential \
        libc-dev \
        g++ \
        locales \
        mercurial \ 
        git \ 
        python2.7 \
        python-pip \
        python-setuptools  \
        python-dev \
        # app
        libzmq3-dev

RUN pip install \
    cython==0.20 \
    gevent==1.0.1 \
    pykka==1.2.0 \
    pyzmq==14.4.1 \
    ipython \
    git+https://github.com/Cougar/pymodbus.git@devel \
    #pymodbus \
    pudb
    

RUN ulimit -n 8192

COPY ./pudb.cfg /root/.config/pudb/pudb.cfg

VOLUME /source

WORKDIR /source

# docker build -t="qg_bal:0.1" .
